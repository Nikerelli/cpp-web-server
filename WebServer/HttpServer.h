#pragma once
#include <string.h>
#include <iostream>

#include <WS2tcpip.h>

namespace WebServer {
	using namespace std;

	class HttpServer {
	public:
		int start(const char* address = "127.0.0.1", const char* port = "80");
	};
}
