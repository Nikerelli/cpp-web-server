#include <iostream>
#include <sstream>
#include <string.h>
#include <thread>
#include <vector>
#include "HttpServer.h"

#define _WIN32_WINNT 0x501

#include <WinSock2.h>
#include <WS2tcpip.h>

#pragma comment(lib, "Ws2_32.lib")


namespace WebServer {
	using namespace std;

	void handleRequest(int client_socket);

	const int DefaultDataSegmentSize = 3072;

	int HttpServer::start(const char* host, const char* port)
	{
		WSADATA wsaData;
		int result = WSAStartup(MAKEWORD(2, 2), &wsaData);

		if (result != 0) {
			cerr << "WSAStartup failed: " << result << "\n";
		}

		struct addrinfo* addr;
		struct addrinfo hints;
		ZeroMemory(&hints, sizeof(hints));

		hints.ai_family = AF_INET;
		hints.ai_socktype = SOCK_STREAM;
		hints.ai_protocol = IPPROTO_TCP;
		hints.ai_flags = AI_PASSIVE;

		result = getaddrinfo(host, port, &hints, &addr);
		if (result != 0) {
			cerr << "getaddrinfo failed: " << result << "\n";
		}

		int listen_socket = socket(addr->ai_family, addr->ai_socktype, addr->ai_protocol);

		if (listen_socket == INVALID_SOCKET) {
			cerr << "Error at socket: " << WSAGetLastError() << "\n";
			freeaddrinfo(addr);
			WSACleanup();
			delete addr;
			return 1;
		}

		result = bind(listen_socket, addr->ai_addr, (int)addr->ai_addrlen);

		if (result == SOCKET_ERROR) {
			cerr << "bind failed with error: " << WSAGetLastError() << "\n";
			freeaddrinfo(addr);
			closesocket(listen_socket);
			WSACleanup();
			delete addr;
			return 1;
		}

		if (listen(listen_socket, 20) == SOCKET_ERROR) {
			cerr << "listen failed with error: " << WSAGetLastError() << "\n";
			closesocket(listen_socket);
			WSACleanup();
			delete addr;
			return 1;
		}

		while (true) {
			int client_socket = accept(listen_socket, NULL, NULL);
			if (client_socket == INVALID_SOCKET) {
				cerr << "accept failed: " << WSAGetLastError() << "\n";
				closesocket(client_socket);
			}

			thread reqThread(handleRequest, client_socket);
			reqThread.detach();
		}

		closesocket(listen_socket);
		freeaddrinfo(addr);
		WSACleanup();
		return 0;
	}

	void handleRequest(int client_socket) {
		vector<string> strings(2);
		int result = DefaultDataSegmentSize;
		while (result == DefaultDataSegmentSize) {
			char* buff = new char[DefaultDataSegmentSize];
			result = recv(client_socket, buff, DefaultDataSegmentSize - 1, 0);
			buff[result] = '\0';
			strings.push_back(string(buff));
		}

		

		stringstream responseBody;

		responseBody << "<title>Test C++ HTTP Server</title>\n"
			<< "<h1>Test page</h1>\n"
			<< "<p>This is body of the test page...</p>\n"
			<< "<h2>Request headers</h2>\n"
			<< "<pre>";

		for (const auto &str : strings) {
			responseBody << str;
		}

		responseBody << "</pre>\n"
			<< "<em><small>Test C++ Http Server</small></em>\n";


		stringstream response;
		response << "HTTP/1.1 200 OK\r\n"
			<< "Version: HTTP/1.1\r\n"
			<< "Content-Type: text/html; charset=utf-8\r\n"
			<< "Content-Length: " << responseBody.str().length()
			<< "\r\n\r\n"
			<< responseBody.str();

		result = send(client_socket, response.str().c_str(), response.str().length(), 0);

		if (result == SOCKET_ERROR) {
			cerr << "send failed: " << WSAGetLastError() << "\n";
		}

		closesocket(client_socket);
	}

}